import firebase from 'firebase/compat/app';
import "firebase/compat/firestore";
import 'firebase/compat/auth';
import 'firebase/compat/storage';
const firebaseConfig = {
    apiKey: "AIzaSyDjQ-C9TbL-yOgczA29sHI0nExvTNVL8cM",
    authDomain: "proyecto-32946.firebaseapp.com",
    projectId: "proyecto-32946",
    storageBucket: "proyecto-32946.appspot.com",
    messagingSenderId: "936789234331",
    appId: "1:936789234331:web:372e8aeacd4e1e8b722c3e"
  };
  
const firebaseApp = firebase.initializeApp(firebaseConfig)
const auth = firebase.auth()
const storage = firebase.storage()
const db = firebaseApp.firestore()
export { auth, db, storage }